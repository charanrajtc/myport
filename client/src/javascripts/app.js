/**
 * crossoverAuction 
 * @author Charan Raj T C <charanraj.tc@gmail.com>
 */


// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

//Doument ready function 
$(document).ready(function(){
	console.log("Hello Document Loaded ");
});


//adding underscore to angular http://underscorejs.org/
var underscore = angular.module('underscore', []);
underscore.factory('_', [function() {
    //return the underscore object 
    return window._; //Underscore must already be loaded on the page
}]);


//initialize angular application here 

var Myport = angular.module('Myport', ['underscore','lbServices']);


// maincontroller for the application 

Myport.controller('mainController', ['$rootScope', '$scope','Mmt', function($rootScope, $scope,Mmt) {
    console.log("Main contoller loaded ");   
    //initalize empty array 
    $scope.Mmts =[];
    $scope.selectedMmt = [];
    $scope.searchtext="";

    // no of air ports per page 
    $scope.pageLimit=10;
    $scope.TotalCount=0;

    $scope.fetcthData=function(){
        console.log($scope.Mmts.length);
        Mmt
          .find({filter:{limit: $scope.pageLimit,offset:$scope.Mmts.length}})
          .$promise
          .then(function(Mmts) {
            $scope.Mmts = Mmts;
            $scope.selectedMmt = $scope.selectedShop || Mmts[0];
            // debug line 
            console.log('Airports',Mmts);
          });
    };

    //on controller load fetch data 
    $scope.fetcthData();
    
    //function to show single airport 
    $scope.showDetails=function(mmt){
        console.log('single airport selected',mmt);
        $scope.selectedMmt=mmt;
    };

}]);