// Gruntfile.js
module.exports = function(grunt) {
    grunt.initConfig({
        //read package variables
        pkg: grunt.file.readJSON('package.json'),
        // check all js files for errors
        jshint: {
            //coding standards are not followed and it may result in unstable but code is too much and cannot able to clean every thing
            //added excluions for them 
            frontjs: {
                options: {

                },
                src: ['client/src/javascripts/**/*.js'],
            },
            backjs: {
                options: {
                    node: true,
                    esnext: true,
                    bitwise: true,
                    camelcase: true,
                    eqeqeq: true,
                    eqnull: true,
                    immed: true,
                    indent: 2,
                    latedef: "nofunc",
                    newcap: true,
                    nonew: true,
                    noarg: true,
                    quotmark: "single",
                    regexp: true,
                    undef: true,
                    unused: false,
                    trailing: true,
                    sub: true,
                    maxlen: 80

                },
                src: ['server/**/*.js'],
            }

        },

        // Third party libraries concat only 
        concat: {
            jsconcat: {
                options: {
                    separator: '\n;\n',
                    nonull: true,
                    stripBanners: { line: true, block: false },
                },
                files: {
                    'client/dist/javascripts/dist.tplibs.min.js': ["bower_components/jquery/dist/jquery.min.js", "bower_components/underscore/underscore-min.js", "bower_components/bootstrap/dist/js/bootstrap.min.js", "bower_components/angular/angular.min.js","bower_components/angular-resource/angular-resource.min.js"],
                },
            },
            cssconcat: {
                options: {
                    separator: '\n\n',
                    nonull: true,
                    stripBanners: { line: true, block: false },
                },
                files: {
                    'client/dist/stylesheets/dist.tplibs.min.css': ["bower_components/bootstrap/dist/css/bootstrap.min.css", "bower_components/font-awesome/css/font-awesome.min.css"],
                },
            },
        },


        // take all the js files and minify them into dist.app.min.js
        uglify: {
            build: {
                files: {
                    'client/dist/javascripts/dist.app.min.js': ['client/src/javascripts/**/*.js']
                },
                // options:{
                //     mangle :false
                // }
            }
        },
        // take the processed style.css file and minify
        cssmin: {
            build: {
                files: {
                    'client/dist/stylesheets/dist.style.min.css': ['client/src/stylesheets/**/*.css']
                }
            }
        },
        // copy fonts an images to dist folder in the client side and other html 
        copy: {
            main: {
                files: [
                    // includes files within path and its sub-directories
                    { expand: true, src: ['**'], cwd: 'client/src/fonts/', dest: 'client/dist/fonts/' },
                    //copy any root html or ther files 
                    { expand: true, src: ['*'], cwd: 'client/src/', dest: 'client/dist/', filter: 'isFile' },
                ],
            },
        },

        clean: {
            log: ["log/*.log"],
            dist: ["client/dist/*.*","client/dist/fonts/**"]
        },

        // watch css and js files and process the above tasks
        watch: {
            css: {
                files: ['client/src/stylesheets/**/*.css'],
                tasks: ['cssmin']
            },
            jscheck: {
                files: ['client/src/javascripts/**/*.js', 'server/**/*.js'],
                tasks: ['jshint']
            },
            jsmin: {
                files: ['client/src/javascripts/**/*.js'],
                tasks: ['uglify']
            },
            clean_copy: {
                files: ['client/src/**'],
                tasks: ['clean:dist','copy']
            }
        },
        // configure nodemon        
        nodemon: {
            dev: {
                script: '<%= pkg.scripts.start %>',
                options: {
                    // get envirnoment from the package.json
                    env: '<%= pkg.env %>',

                }
            }
        },
        // run watch and nodemon at the same time
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            tasks: ['nodemon', 'watch']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-concurrent');


    // load nodemon
    grunt.loadNpmTasks('grunt-nodemon');

    // register the nodemon task when we run grunt
    grunt.registerTask('default', ['clean', 'cssmin', 'concat', 'jshint', 'uglify', 'copy', 'concurrent']);

};
